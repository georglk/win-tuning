function Get-RegistryKeyValue {
	[CmdletBinding()]
	param(
		[Parameter(Mandatory=$true)]
		[ValidateNotNullOrEmpty()]
		[string]
		$Path,
		
		[Parameter(Mandatory=$true)]
		[ValidateNotNullOrEmpty()]
		[string]
		$Name
	)

	try {
		$value = (Get-ItemProperty -Path $Path -ErrorAction Stop | Select-Object -ExpandProperty $Name -ErrorAction Stop)
		return $value
	}
	catch {
		return $null
	}
}

function Add-RegistryKeyValue {
	[CmdletBinding()]
	param(
		[Parameter(Mandatory = $true)]
		[ValidateNotNullOrEmpty()]
		[string]
		$Path,
		
		[Parameter(Mandatory = $true)]
		[ValidateNotNullOrEmpty()]
		[string]
		$Name,

		[Parameter(Mandatory = $true)]
		[ValidateNotNullOrEmpty()]
		[string]
		$Value,

		[Parameter()]
		[ValidateSet("String", "ExpandString", "Binary", "DWord", "MultiString", "Qword", "Unknown")]
		[string]
		$Type = "String"
	)

	if (-not (Test-Path -Path $Path)) {
		try{
			New-item -Path $Path -Force -ErrorAction Stop | Out-Null
		}
		catch {
			return $null
		}
	}

	try {
		Set-ItemProperty -Path $Path -Name $Name -Value $Value -Type $Type -ErrorAction Stop
		return $true
	}
	catch {
		return $null
	}
}

function Set-RegistryKeyValue {
	[CmdletBinding()]
	param(
		[Parameter(Mandatory = $true)]
		[ValidateNotNullOrEmpty()]
		[string]
		$Path,
		
		[Parameter(Mandatory = $true)]
		[ValidateNotNullOrEmpty()]
		[string]
		$Name,

		[Parameter(Mandatory = $true)]
		[ValidateNotNullOrEmpty()]
		[string]
		$Value,

		[Parameter()]
		[ValidateSet("String", "ExpandString", "Binary", "DWord", "MultiString", "Qword", "Unknown")]
		[string]
		$Type = "String"
	)

	if ((Get-RegistryKeyValue -Path $Path -Name $Name) -eq $Value)
	{
		return $false
	}

	if ($null -eq (Add-RegistryKeyValue @PSBoundParameters))
	{
		return $null
	}

	return $true
}

function Get-Shortcut {
<#
.SYNOPSIS
	Get information about a Shortcut (.lnk file)
.DESCRIPTION
	Get information about a Shortcut (.lnk file)
.PARAMETER Path
	Path to .lnk file
.EXAMPLE
	Get-Shortcut -path C:\portable\test2.lnk

	LinkPath     : C:\portable\test2.lnk
	Link         : test2.lnk
	TargetPath   : C:\Windows\System32\ncpa.cpl
	Target       : ncpa.cpl
	Arguments    :
	Hotkey       :
	WindowStyle  : Normal
	IconLocation : %SystemRoot%\system32\ncpa.cpl,0
	RunAsAdmin   : False
.NOTES
	Updates:
	* added code to determine RunAsAdmin status
	* added code to display WindowStyle as text as opposed to an integer

	Main function inspired by:
	https://stackoverflow.com/questions/484560/editing-shortcut-lnk-properties-with-powershell

	Checking for RunAsAdmin inspired by:
	https://community.idera.com/database-tools/powershell/powertips/b/tips/posts/managing-shortcut-files-part-3
#>

	[CmdletBinding(ConfirmImpact='None')]
	param(
		[string] $Path
	)

	begin {
		Write-Verbose -Message "Starting [$($MyInvocation.Mycommand)]"
		$Obj = New-Object -ComObject WScript.Shell
	}

	process {
		if (Test-Path -Path $Path) {
			[array] $ResolveFile = Resolve-Path -Path $Path
			if ($ResolveFile.count -gt 1) {
				Write-Error -Message "ERROR: File specification [$File] resolves to more than 1 file."
			} else {
				Write-Verbose -Message "Using file [$ResolveFile] in section [$Section], getting comments"
				$ResolveFile = Get-Item -Path $ResolveFile
				if ($ResolveFile.Extension -eq '.lnk') {
					$Link = $Obj.CreateShortcut($ResolveFile.FullName)

					$Info = ([ordered] @{})
					$Info.LinkPath = $Link.FullName
					$Info.Link = try { Split-Path -Path $Info.LinkPath -Leaf } catch { 'n/a'}

					$Info.TargetPath = $Link.TargetPath
					$Info.Target = try {Split-Path -Path $Info.TargetPath -Leaf } catch { 'n/a'}
					$Info.Arguments = $Link.Arguments
					$Info.Hotkey = $Link.Hotkey
					$Info.WindowStyle = $( switch($Link.WindowStyle) {
						1 { 'Normal' }
						3 { 'Maximized' }
						7 { 'Minimized' }
					})
					$Info.IconLocation = $Link.IconLocation
					$Info.RunAsAdmin = $(
						$Bytes = [System.IO.File]::ReadAllBytes($ResolveFile)
						if ($Bytes[0x15] -band 0x20) { $true } else { $false }
					)
					$Info.Description = $Link.Description
					New-Object -TypeName PSObject -Property $Info
				} else {
					Write-Error -Message 'Extension is not .lnk'
				}
			}
		} else {
			Write-Error -Message "ERROR: File [$Path] does not exist"
		}
	}

	end {
		Write-Verbose -Message "Ending [$($MyInvocation.Mycommand)]"
	}
}

function Remove-RegistryKey {
	[CmdletBinding()]
	param (
		[Parameter(Mandatory = $true)]
		[ValidateNotNullOrEmpty()]
		[string]
		$Path
	)

	if (Test-Path -Path $Path)
	{
		try {
			Remove-Item -Recurse -Force -Path $Path -ErrorAction Stop
			return $true
		}
		catch {
			return $null
		}
	}
	else
	{
		return $false
	}
}

function New-Shortcut {
<#
.SYNOPSIS
	This script is used to create a  shortcut.
.DESCRIPTION
	This script uses a Com Object to create a shortcut.
.PARAMETER Path
	The path to the shortcut file.  .lnk will be appended if not specified.  If the folder name doesn't exist, it will be created.
.PARAMETER TargetPath
	Full path of the target executable or file.
.PARAMETER Arguments
	Arguments for the executable or file.
.PARAMETER Description
	Description of the shortcut.
.PARAMETER HotKey
	Hotkey combination for the shortcut.  Valid values are SHIFT+F7, ALT+CTRL+9, etc.  An invalid entry will cause the function to fail.
.PARAMETER WorkDir
	Working directory of the application.  An invalid directory can be specified, but invoking the application from the shortcut could fail.
.PARAMETER WindowStyle
	Windows style of the application, Normal (1), Maximized (3), or Minimized (7).  Invalid entries will result in Normal behavior.
.PARAMETER IconLocation
	Full path of the icon file.  Executables, DLLs, etc with multiple icons need the number of the icon to be specified, otherwise the first icon will be used, i.e.:  c:\windows\system32\shell32.dll,99
.PARAMETER RunAsAdmin
	Used to create a shortcut that prompts for admin credentials when invoked, equivalent to specifying runas.
.PARAMETER Interactive
	Switch that will display the shortcut just created.
.NOTES
	* Added -Interactive switch to display created shortcut
	* Updated -WindowStyle to accept readable content of 'Normal', 'Maximized', 'Minimized' and write correct integer values to shortcut
	* Updated -IconLocation renamed from -Icon to match the output of Get-Shortcut
	* Updated -RunAsAdmin renamed from -Admin and altered code to make more consistent

	Main logic inspired by:
	https://gallery.technet.microsoft.com/scriptcenter/New-Shortcut-4d6fb3d8

	Run as admin inspired by:
	https://community.idera.com/database-tools/powershell/powertips/b/tips/posts/managing-shortcut-files-part-3
.INPUTS
	Strings and Integer
.OUTPUTS
	[psobject]
.EXAMPLE
	New-Shortcut -Path c:\temp\notepad.lnk -TargetPath c:\windows\notepad.exe -Interactive

	Creates a simple shortcut to Notepad at c:\temp\notepad.lnk Function would return:

	LinkPath     : C:\temp\notepad.lnk
	Link         : notepad.lnk
	TargetPath   : C:\Windows\notepad.exe
	Target       : notepad.exe
	Arguments    :
	Hotkey       :
	WindowStyle  : Normal
	IconLocation : ,0
	RunAsAdmin   : False
	Description  :
.EXAMPLE
	New-Shortcut "$($env:Public)\Desktop\Notepad" c:\windows\notepad.exe -WindowStyle 3 -RunAsAdmin

	Creates a shortcut named Notepad.lnk on the Public desktop to notepad.exe that launches maximized after prompting for admin credentials.
.EXAMPLE
	New-Shortcut "$($env:USERPROFILE)\Desktop\Notepad.lnk" c:\windows\notepad.exe -IconLocation "c:\windows\system32\shell32.dll,99"

	Creates a shortcut named Notepad.lnk on the user's desktop to notepad.exe that has a pointy finger icon (on Windows 7).
.EXAMPLE
	New-Shortcut "$($env:USERPROFILE)\Desktop\Notepad.lnk" c:\windows\notepad.exe C:\instructions.txt

	Creates a shortcut named Notepad.lnk on the user's desktop to notepad.exe that opens C:\instructions.txt
.EXAMPLE
	New-Shortcut "$($env:USERPROFILE)\Desktop\ADUC" %SystemRoot%\system32\dsa.msc -Admin

	Creates a shortcut named ADUC.lnk on the user's desktop to Active Directory Users and Computers that launches after prompting for admin credentials
.EXAMPLE
	New-Shortcut -Path F:\DNE\notepad.lnk -TargetPath c:\windows\notepad.exe -Interactive

	If run on a system that does NOT have an F: drive it will return the following:

	New-Shortcut : Unable to create [f:\DNE], shortcut cannot be created
	At line:1 char:1
	+ New-Shortcut -Path f:\DNE\notepad.lnk -TargetPath c:\windows\notepad. ...
	+ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		+ CategoryInfo          : NotSpecified: (:) [Write-Error], WriteErrorException
		+ FullyQualifiedErrorId : Microsoft.PowerShell.Commands.WriteErrorException,New-Shortcut
#>

	#region Parameters
	[CmdletBinding(SupportsShouldProcess)]
	[OutputType('psobject')]
	param(
		[Parameter(Mandatory,HelpMessage='Enter the path to the shortcut you want to create/update',  ValueFromPipelineByPropertyName,Position=0)]
		[Alias('File','Shortcut')]
		[string] $Path,

		[Parameter(Mandatory,HelpMessage='Enter the path to the program or file you want to run',  ValueFromPipelineByPropertyName,Position=1)]
		[string] $TargetPath,

		[Parameter(ValueFromPipelineByPropertyName,Position=2)]
		[Alias('Args')]
		[string] $Arguments,

		[Parameter(ValueFromPipelineByPropertyName,Position=3)]
		[string] $Description,

		[Parameter(ValueFromPipelineByPropertyName,Position=4)]
		[string] $HotKey,

		[Parameter(ValueFromPipelineByPropertyName,Position=5)]
		[Alias('WorkingDirectory','WorkingDir')]
		[string] $WorkDir,

		[Parameter(ValueFromPipelineByPropertyName,Position=6)]
		[ValidateSet('1', 'Normal', 3, 'Maximized', 7, 'Minimized')]
		[string] $WindowStyle = 'Normal',

		[Parameter(ValueFromPipelineByPropertyName,Position=7)]
		[string] $IconLocation,

		[Parameter(ValueFromPipelineByPropertyName)]
		[switch] $RunAsAdmin,

		[switch] $Interactive
	)
	#endregion Parameters

	begin {
		Write-Verbose -Message "Starting [$($MyInvocation.Mycommand)]"
	}

	process {
		If (!($Path -match '^.*(\.lnk)$')) {
			$Path = "$Path`.lnk"
		}
		[System.IO.FileInfo] $Path = $Path
		$ShouldMessage = "WHATIF: Would create SHORTCUT [$($path.fullname)] ARGUMENTS [$($Arguments)] DESCRIPTION [$($Description)] HOTKEY [$($HotKey)]`nWORKDIR [$($WorkDir)] WINDOWSTYLE [$($WindowStyle)] ICON [$($IconLocation)]"
		if ($PSCmdlet.ShouldProcess($ShouldMessage))
		{
			try {
				If (!(Test-Path -Path $Path.DirectoryName)) {
					$null = mkdir -Path $Path.DirectoryName -ErrorAction Stop
				}
			} catch {
				Write-Error -Message "Unable to create [$($Path.DirectoryName)], shortcut cannot be created"
				break
			}
			# Define Shortcut Properties
			$WshShell = New-Object -ComObject WScript.Shell
			$Shortcut = $WshShell.CreateShortcut($Path.FullName)
			$Shortcut.TargetPath = $TargetPath
			$Shortcut.Arguments = $Arguments
			$Shortcut.Description = $Description
			$Shortcut.HotKey = $HotKey
			$Shortcut.WorkingDirectory = $WorkDir
			switch ($WindowStyle) {
				'Normal' { $WindowStyle = 1 }
				'Maximized' { $WindowStyle = 3 }
				'Minimized' { $WindowStyle = 7 }
			}
			$Shortcut.WindowStyle = $WindowStyle
			if ($IconLocation){
				$Shortcut.IconLocation = $IconLocation
			}
			try {
				$Shortcut.Save()
				$Bytes = [System.IO.File]::ReadAllBytes($Path.FullName)
				if ($RunAsAdmin) {
					$bytes[0x15] = $bytes[0x15] -bor 0x20
				} else {
					$bytes[0x15] = $bytes[0x15] -band -not 0x20
				}
				[System.IO.File]::WriteAllBytes($path.FullName, $bytes)
				if ($Interactive) {
					Get-Shortcut -Path $Path.FullName
				}
			} catch {
				Write-Error -Message "Unable to create shortcut [$($Path.FullName)]"
				break
			}
		}
	}

	end {
		Write-Verbose -Message "Ending [$($MyInvocation.Mycommand)]"
	}
}

function Restart-Explorer {
	# Save all opened folders in order to restore them after File Explorer restart
	$Script:OpenedFolders = {(New-Object -ComObject Shell.Application).Windows() | ForEach-Object -Process {$_.Document.Folder.Self.Path}}.Invoke()

	Stop-Process -Name explorer -Force
	Start-Sleep -Seconds 3

	# Restoring closed folders
	foreach ($Script:OpenedFolder in $Script:OpenedFolders)
	{
		if (Test-Path -Path $Script:OpenedFolder)
		{
			Start-Process -FilePath explorer -ArgumentList $Script:OpenedFolder
		}
	}
}

function Hide-FoldersfromThisPC {
<#
	Документы (Documents) f42ee2d3-909f-4907-8871-4c22fc0bf756
	Загрузки (Downloads) 7d83ee9b-2244-4e70-b1f5-5393042af1e4
	Изображения (Pictures) 0ddd015d-b06c-45d5-8c4c-f59713854639
	Музыка (Music) a0c69a99-21c8-4671-8703-7934162fcf1d
	Рабочий стол (Desktop) B4BFCC3A-DB2C-424C-B029-7FE99A87C641
	Объемные объекты (3D Objects) 31C0DD25-9439-4F12-BF41-7FF4EDA38722
	Видео (Videos) 35286a68-3c57-41a1-bbb1-0eae73d76c95

	Hide-FoldersfromThisPC -folder Pictures -Hide
	Hide-FoldersfromThisPC -folder Music -Hide
	Hide-FoldersfromThisPC -folder Videos -Hide
#>
	param
	(

		[Parameter(Mandatory = $true)]
		[ValidateSet("Documents", "Downloads", "Pictures", "Music", "Desktop", "3DObjects", "Videos")]
		[string]
		$folder,

		[Parameter(
			Mandatory = $true,
			ParameterSetName = "Show"
		)]
		[switch]
		$Show,

		[Parameter(
			Mandatory = $true,
			ParameterSetName = "Hide"
		)]
		[switch]
		$Hide
	)

	Switch ($folder) {
		"Documents" { $guid = 'f42ee2d3-909f-4907-8871-4c22fc0bf756' }
		"Downloads" { $guid = '7d83ee9b-2244-4e70-b1f5-5393042af1e4' }
		"Pictures" { $guid = '0ddd015d-b06c-45d5-8c4c-f59713854639' }
		"Music" { $guid = 'a0c69a99-21c8-4671-8703-7934162fcf1d' }
		"Desktop" { $guid = 'B4BFCC3A-DB2C-424C-B029-7FE99A87C641' }
		"3DObjects" { $guid = '31C0DD25-9439-4F12-BF41-7FF4EDA38722' }
		"Videos" { $guid = '35286a68-3c57-41a1-bbb1-0eae73d76c95' }
	}

	switch ($PSCmdlet.ParameterSetName)
	{
		"Hide"
		{
			if (-not (Test-Path -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\FolderDescriptions\{$guid}\PropertyBag"))
			{
				New-Item -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\FolderDescriptions\{$guid}\PropertyBag" -Force
			}
			New-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\FolderDescriptions\{$guid}\PropertyBag" -Name ThisPCPolicy -PropertyType String -Value Hide -Force
		}
		"Show"
		{
			Remove-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\FolderDescriptions\{$guid}\PropertyBag" -Name ThisPCPolicy -Force -ErrorAction SilentlyContinue
		}
	}

	# In order for the changes to take effect the File Explorer process has to be restarted
	# Чтобы изменения вступили в силу, необходимо перезапустить процесс проводника
	Restart-Explorer
}

function Copy-FromUri {
	[CmdletBinding()]
	param (
		$Uri,
		$DestPath,
		$TempPath
	)

	if ([System.IO.File]::Exists($DestPath))
	{
		Start-BitsTransfer -Source $Uri -Destination $TempPath -DisplayName "Download $Uri" -ErrorAction Stop
		if ($(Get-FileHash -Path "$TempPath" -Algorithm SHA256).Hash -ne $(Get-FileHash -Path "$DestPath" -Algorithm SHA256).Hash) {
			Copy-Item -Path "$TempPath" -Destination "$DestPath" -Force
		}
	}
	else
	{
		Start-BitsTransfer -Source $Uri -Destination $DestPath -DisplayName "Download $Uri" -ErrorAction Stop
	}
}

function Show-Result {
	param (
		$Parameters, $Result
	)

	switch ($Result)
	{
		$true
		{
			"==> Set $($Parameters.Name) = $($Parameters.Value) ... OK" | Write-Host -ForegroundColor Yellow
		}

		$null
		{
			"==> Set $($Parameters.Name) = $($Parameters.Value) ... Error" | Write-Host -ForegroundColor Red
		}

		$false {
			# Do nothing
		}

		default
		{
			"==> Result ... Unknown" | Write-Host -ForegroundColor Red
		}
	}
}