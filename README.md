# win-tuning

[[_TOC_]]

## PowerShell Execution Policies

https://learn.microsoft.com/en-us/powershell/module/microsoft.powershell.security/set-executionpolicy

```powershell
Set-ExecutionPolicy -Scope CurrentUser -ExecutionPolicy Bypass -Force
```

## PS Module win-tuning

```powershell
$Parameters = @{
	Uri = 'https://gitlab.com/georglk/win-tuning/-/raw/main/Module/win-tuning.psm1'
	OutFile = "$env:TEMP\win-tuning.psm1"
	UseBasicParsing = $true
}

Invoke-RestMethod @Parameters

$Module_Dir = [System.IO.Path]::Combine([Environment]::GetFolderPath('MyDocuments'), 'WindowsPowerShell', 'Modules', 'win-tuning')
if (-not (Test-Path -PathType Container -Path $Module_Dir)) { New-Item -ItemType "directory" -Path $Module_Dir | Out-Null }
Copy-Item -Path $Parameters.OutFile -Destination $Module_Dir -Force
```

## Разное

```powershell
Invoke-RestMethod 'https://gitlab.com/georglk/win-tuning/-/raw/main/scripts/shortcut.ps1' | Invoke-Expression

Invoke-RestMethod 'https://gitlab.com/georglk/win-tuning/-/raw/main/scripts/other.ps1' | Invoke-Expression

Invoke-RestMethod 'https://gitlab.com/georglk/win-tuning/-/raw/main/scripts/sound.ps1' | Invoke-Expression
```

## Set as desktop background

Remove "Set as desktop background"

```powershell
Invoke-RestMethod 'https://gitlab.com/georglk/win-tuning/-/raw/main/scripts/set-as-desktop-background.ps1' -OutFile "$env:TEMP\set-as-desktop-background.ps1"

. "$env:TEMP\set-as-desktop-background.ps1"
```

## Языковые и региональные настройки

```powershell
Invoke-RestMethod 'https://gitlab.com/georglk/win-tuning/-/raw/main/scripts/language.ps1' -OutFile "$env:TEMP\language.ps1"

. "$env:TEMP\language.ps1"
```

## Microsoft Windows Update

```powershell
Invoke-RestMethod 'https://gitlab.com/georglk/win-tuning/-/raw/main/scripts/windowsupdate.ps1' -OutFile "$env:TEMP\windowsupdate.ps1"

. "$env:TEMP\windowsupdate.ps1"
```

## Microsoft Edge

```powershell
Invoke-RestMethod 'https://gitlab.com/georglk/win-tuning/-/raw/main/scripts/microsoft-edge.ps1' -OutFile "$env:TEMP\microsoft-edge.ps1"

. "$env:TEMP\microsoft-edge.ps1"
```

## Sophia Script for Windows

https://github.com/farag2/Sophia-Script-for-Windows

```powershell
Invoke-RestMethod 'https://gitlab.com/georglk/win-tuning/-/raw/main/scripts/sophia-environment.ps1' -OutFile "$env:TEMP\sophia-environment.ps1"

. "$env:TEMP\sophia-environment.ps1"

Invoke-RestMethod 'https://gitlab.com/georglk/win-tuning/-/raw/main/scripts/sophia-call.ps1' | Invoke-Expression
```

## Google Chrome

```powershell
Invoke-RestMethod 'https://gitlab.com/georglk/win-tuning/-/raw/main/scripts/chrome-install.ps1' -OutFile "$env:TEMP\chrome-install.ps1"

. "$env:TEMP\chrome-install.ps1"

Invoke-RestMethod 'https://gitlab.com/georglk/win-tuning/-/raw/main/scripts/chrome-settings.ps1' -OutFile "$env:TEMP\chrome-settings.ps1"

. "$env:TEMP\chrome-settings.ps1"
```

## Visual Studio Code (VS Code)

```powershell
Invoke-RestMethod 'https://gitlab.com/georglk/win-tuning/-/raw/main/scripts/vscode-install.ps1' | Invoke-Expression
Invoke-RestMethod 'https://gitlab.com/georglk/win-tuning/-/raw/main/scripts/vscode-settings.ps1' | Invoke-Expression
```

## Manual

### Веб-браузер по умолчанию

Открыть "Приложения по умолчанию"

```
explorer.exe shell:::{17cd9488-1228-4b2f-88ce-4298e93e0966} -Microsoft.DefaultPrograms\pageDefaultProgram
```

### VisualEffects

Открыть "Параметры быстродействия" (Performance Options)

```powershell
. "$env:windir\system32\SystemPropertiesPerformance.exe"
```

Отключаем всё кроме:

+ Отображение теней, отбрасываемых окнами (Show shadows under windows)
+ Отображения тени под указателем мышки (Show shadows under mouse pointer)
+ Сглаживание неровностей экранных шрифтов (Smooth edges of screen fonts)

### Google Chrome

Настроить Chrome → Показывать ярлыки (Show shortcuts): выключить (turn off)

### Уведомления и действия (Notifications & actions)

### Шрифт в Windows PowerShell ISE

### Шрифт в Командная строка (cmd.exe)

### Шрифт в Windows PowerShell (powershell.exe)
