"==> region Privacy & Telemetry" | Write-Host -ForegroundColor Yellow
DiagTrackService -Disable
DiagnosticDataLevel -Minimal
ErrorReporting -Disable
FeedbackFrequency -Never
ScheduledTasks -Disable
AdvertisingID -Disable
WindowsTips -Disable
AppsSilentInstalling -Disable
WhatsNewInWindows -Disable
TailoredExperiences -Disable
BingSearch -Disable

"==> region UI & Personalization" | Write-Host -ForegroundColor Yellow
ThisPC -Show
FileExtensions -Show
FileExplorerRibbon -Expanded
OneDriveFileExplorerAd -Hide
3DObjects -Hide
TaskbarSearch -SearchIcon
CortanaButton -Hide
NewsInterests -Disable
PeopleTaskbar -Hide
UnpinTaskbarShortcuts -Shortcuts Edge, Store, Mail
JPEGWallpapersQuality -Max
TaskManagerWindow -Expanded
ShortcutsSuffix -Disable

"==> region OneDrive" | Write-Host -ForegroundColor Yellow
OneDrive -Uninstall

"==> region System" | Write-Host -ForegroundColor Yellow
StickyShift -Disable
Autoplay -Disable
UninstallPCHealthCheck
InstallVCRedist
InstallDotNetRuntimes

"==> region Start menu" | Write-Host -ForegroundColor Yellow
PinToStart -UnpinAll

"==> region UWP apps" | Write-Host -ForegroundColor Yellow
UninstallUWPApps
CortanaAutostart -Disable

"==> region Gaming" | Write-Host -ForegroundColor Yellow
XboxGameBar -Disable
XboxGameTips -Disable

"==> region Scheduled tasks" | Write-Host -ForegroundColor Yellow
CleanupTask -Register
SoftwareDistributionTask -Register
TempTask -Register

"==> region Microsoft Defender & Security" | Write-Host -ForegroundColor Yellow
DismissMSAccount
DismissSmartScreenFilter
# AuditProcess -Enable
# CommandLineProcessAudit -Enable
# EventViewerCustomView -Enable
AppsSmartScreen -Disable
SaveZoneInformation -Disable

"==> region Context menu" | Write-Host -ForegroundColor Yellow
EditWithPaint3DContext -Hide
EditWithPhotosContext -Hide
CreateANewVideoContext -Hide
PrintCMDContext -Hide

"Changes take effect after the computer is restarted" | Write-Host -ForegroundColor Red -NoNewline
" " | Write-Host -NoNewline
1..3 | ForEach-Object { "." | Write-Host -ForegroundColor Red -NoNewline; Start-Sleep -Second 1 }
" " | Write-Host -NoNewline
"Restart" | Write-Host -ForegroundColor DarkRed
Start-Sleep -Second 1
Restart-Computer
