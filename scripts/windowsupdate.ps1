#Requires -RunAsAdministrator

<#
https://learn.microsoft.com/en-us/windows/win32/api/wuapi/nf-wuapi-iupdatesearcher-search
#>

# $ErrorActionPreference = 'Stop'

# Enum definition
$operationResultCode = @(
	'Not Started'
	'In Progress'
	'Succeeded'
	'Succeeded With Errors'
	'Failed'
	'Aborted'
)

## Part 1: Search Update

try
{
	$updateSession  = New-Object -ComObject 'Microsoft.Update.Session'
	$updateSearcher = $updateSession.CreateUpdateSearcher()
	$searchResult = $updateSearcher.Search("IsInstalled=0 and Type='Software' and IsAssigned=1 and IsHidden=0")

	$updateDownload = New-Object -ComObject 'Microsoft.Update.UpdateColl'

	foreach ($currentUpdate in $searchResult.Updates)
	{
		$currentUpdate.Title
		if (-not $currentUpdate.EulaAccepted)
		{
			$currentUpdate.EulaAccepted = $true
		}
		$updateDownload.Add($currentUpdate) | Out-Null
	}

	if ($updateDownload.Count -eq 0)
	{
		throw 'No updates found'
	}

	'{0} update(s) selected, download now...' -f $updateDownload.Count
}
catch
{
	[String] $_
	exit
}

## Part 2: Download Updates

Start-Sleep -Seconds 2

try
{
	$updateDownloader = $updateSession.CreateUpdateDownloader()
	$updateDownloader.Updates = $updateDownload
	$downloadResult = $updateDownloader.Download()

	if ($downloadResult.ResultCode -ne 2)
	{
		throw ('Download not succeeded with status {0}' -f $operationResultCode[$downloadResult.ResultCode])
	}

	$updateInstall = New-Object -ComObject 'Microsoft.Update.UpdateColl'

	foreach ($currentUpdate in $searchResult.Updates)
	{
		if ($currentUpdate.IsDownloaded)
		{
			$updateInstall.Add($currentUpdate) | Out-Null
		}
	}

	if ($updateInstall.Count -eq 0)
	{
		throw 'No updates downloaded!'
	}

	'{0} update(s) downloaded, install now...' -f $updateInstall.Count
}
catch
{
	[String] $_
	exit
}

## Part 3: Install Updates

Start-Sleep -Seconds 2

try
{
	$updateInstaller = $updateSession.CreateUpdateInstaller()
	$updateInstaller.ForceQuiet = $true
	$updateInstaller.Updates = $updateInstall
	$installResult = $updateInstaller.Install()

	if ($installResult.ResultCode -ne 2)
	{
		throw ('Install not succeeded with status {0}' -f $operationResultCode[$downloadResult.ResultCode])
	}

	'{0} update(s) installed, reboot {1} required' -f $updateInstall.Count, $(if ($installResult.RebootRequired) { 'is' } else { 'is not' })

	if ($installResult.RebootRequired) {
		"Computer will be restarted" | Write-Host -ForegroundColor Red -NoNewline
		" " | Write-Host -NoNewline
		1..3 | ForEach-Object { "." | Write-Host -ForegroundColor Red -NoNewline; Start-Sleep -Second 1 }
		" " | Write-Host -NoNewline
		"Restart" | Write-Host -ForegroundColor DarkRed
		Start-Sleep -Second 2
		Restart-Computer
	}
}
catch
{
	[String] $_
	exit
}
