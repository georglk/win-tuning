#Requires -RunAsAdministrator

Import-Module 'win-tuning'

# Pin to taskbar "Google Chrome"
if ([System.IO.File]::Exists("$env:ProgramFiles\Google\Chrome\Application\chrome.exe"))
{
	$Uri = 'https://gitlab.com/georglk/win-tools/-/raw/master/syspin/syspin.exe'
	$Parameters = @{
		Uri       = $Uri
		DestPath  = [System.IO.Path]::Combine($env:USERPROFILE, [System.IO.Path]::GetFileName($Uri))
		TempPath  = [System.IO.Path]::Combine($env:TEMP, [System.IO.Path]::GetFileName($Uri))
	}

	Copy-FromUri @Parameters

	"==> Pin to taskbar `"Google Chrome`"" | Write-Host  -ForegroundColor Yellow
	New-Shortcut -Path "$env:USERPROFILE\Google Chrome.lnk" -TargetPath "%ProgramFiles%\Google\Chrome\Application\chrome.exe" -IconLocation "%ProgramFiles%\Google\Chrome\Application\chrome.exe,0" -Description "Google Chrome"
	Start-Process -Wait -WindowStyle Hidden -FilePath "$env:USERPROFILE\syspin.exe" -ArgumentList "`"$env:USERPROFILE\Google Chrome.lnk`"","5386"
}

# DefaultBrowserSettingEnabled
$Parameters = @{
	Name  = 'DefaultBrowserSettingEnabled'
	Value = 1
	Type  = 'DWord'
	Path  = 'Registry::HKCU\SOFTWARE\Policies\Google\Chrome'
}

Show-Result $Parameters $(Set-RegistryKeyValue @Parameters)

# BookmarkBarEnabled
$Parameters = @{
	Name  = 'BookmarkBarEnabled'
	Value = 1
	Type  = 'DWord'
	Path  = 'Registry::HKCU\SOFTWARE\Policies\Google\Chrome'
}

Show-Result $Parameters $(Set-RegistryKeyValue @Parameters)

# ShowAppsShortcutInBookmarkBar
$Parameters = @{
	Name  = 'ShowAppsShortcutInBookmarkBar'
	Value = 0
	Type  = 'DWord'
	Path  = 'Registry::HKCU\SOFTWARE\Policies\Google\Chrome'
}

Show-Result $Parameters $(Set-RegistryKeyValue @Parameters)

# WelcomePageOnOSUpgradeEnabled
$Parameters = @{
	Name  = 'WelcomePageOnOSUpgradeEnabled'
	Value = 0
	Type  = 'DWord'
	Path  = 'Registry::HKCU\SOFTWARE\Policies\Google\Chrome'
}

Show-Result $Parameters $(Set-RegistryKeyValue @Parameters)

# ShowFullUrlsInAddressBar
$Parameters = @{
	Name  = 'ShowFullUrlsInAddressBar'
	Value = 1
	Type  = 'DWord'
	Path  = 'Registry::HKCU\SOFTWARE\Policies\Google\Chrome'
}

Show-Result $Parameters $(Set-RegistryKeyValue @Parameters)

# PromotionalTabsEnabled
$Parameters = @{
	Name  = 'PromotionalTabsEnabled'
	Value = 0
	Type  = 'DWord'
	Path  = 'Registry::HKCU\SOFTWARE\Policies\Google\Chrome'
}

Show-Result $Parameters $(Set-RegistryKeyValue @Parameters)

# PasswordManagerEnabled
$Parameters = @{
	Name  = 'PasswordManagerEnabled'
	Value = 0
	Type  = 'DWord'
	Path  = 'Registry::HKCU\SOFTWARE\Policies\Google\Chrome'
}

Show-Result $Parameters $(Set-RegistryKeyValue @Parameters)

# PrivacySandboxPromptEnabled
$Parameters = @{
	Name  = 'PrivacySandboxPromptEnabled'
	Value = 0
	Type  = 'DWord'
	Path  = 'Registry::HKCU\SOFTWARE\Policies\Google\Chrome'
}

Show-Result $Parameters $(Set-RegistryKeyValue @Parameters)

# PrivacySandboxAdTopicsEnabled
$Parameters = @{
	Name  = 'PrivacySandboxAdTopicsEnabled'
	Value = 0
	Type  = 'DWord'
	Path  = 'Registry::HKCU\SOFTWARE\Policies\Google\Chrome'
}

Show-Result $Parameters $(Set-RegistryKeyValue @Parameters)

# PrivacySandboxSiteEnabledAdsEnabled
$Parameters = @{
	Name  = 'PrivacySandboxSiteEnabledAdsEnabled'
	Value = 0
	Type  = 'DWord'
	Path  = 'Registry::HKCU\SOFTWARE\Policies\Google\Chrome'
}

Show-Result $Parameters $(Set-RegistryKeyValue @Parameters)

# PrivacySandboxAdMeasurementEnabled
$Parameters = @{
	Name  = 'PrivacySandboxAdMeasurementEnabled'
	Value = 0
	Type  = 'DWord'
	Path  = 'Registry::HKCU\SOFTWARE\Policies\Google\Chrome'
}

Show-Result $Parameters $(Set-RegistryKeyValue @Parameters)

# BrowserSignin
$Parameters = @{
	Name  = 'BrowserSignin'
	Value = 1
	Type  = 'DWord'
	Path  = 'Registry::HKCU\SOFTWARE\Policies\Google\Chrome'
}

Show-Result $Parameters $(Set-RegistryKeyValue @Parameters)

# BrowserLabsEnabled
$Parameters = @{
	Name  = 'BrowserLabsEnabled'
	Value = 1
	Type  = 'DWord'
	Path  = 'Registry::HKCU\SOFTWARE\Policies\Google\Chrome'
}

Show-Result $Parameters $(Set-RegistryKeyValue @Parameters)

# SyncDisabled
$Parameters = @{
	Name  = 'SyncDisabled'
	Value = 0
	Type  = 'DWord'
	Path  = 'Registry::HKCU\SOFTWARE\Policies\Google\Chrome'
}

Show-Result $Parameters $(Set-RegistryKeyValue @Parameters)

<#
https://chromeenterprise.google/policies/
#>
