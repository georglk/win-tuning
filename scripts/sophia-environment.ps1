#Requires -RunAsAdministrator

$Parameters = @{
	Uri = "script.sophia.team"
	UseBasicParsing = $true
	OutFile = "$env:TEMP\script.sophia.team.ps1"
}

Invoke-RestMethod @Parameters
Start-Sleep -Second 1

. "$env:TEMP\script.sophia.team.ps1"
Start-Sleep -Second 1

switch ($Version)
{
	"Wrapper"
	{
		$SophiaFolder = "$DownloadsFolder\Sophia_Script_Wrapper_v$LatestRelease"
	}
	"LTSC2019"
	{
		$SophiaFolder = "$DownloadsFolder\Sophia_Script_for_Windows_10_LTSC_2019_v$LatestRelease"
	}
	"LTSC2021"
	{
		$SophiaFolder = "$DownloadsFolder\Sophia_Script_for_Windows_10_LTSC_2021_v$LatestRelease"
	}
	"Windows_10_PowerShell_5.1"
	{
		$SophiaFolder = "$DownloadsFolder\Sophia_Script_for_Windows_10_v$LatestRelease"
	}
	"Windows_10_PowerShell_7"
	{
		$SophiaFolder = "$DownloadsFolder\Sophia_Script_for_Windows_10_PowerShell_7_v$LatestRelease"
	}
	"Windows_11_PowerShell_5.1"
	{
		$SophiaFolder = "$DownloadsFolder\Sophia_Script_for_Windows_11_v$LatestRelease"
	}
	"Windows_11_PowerShell_7"
	{
		$SophiaFolder = "$DownloadsFolder\Sophia_Script_for_Windows_11_PowerShell_7_v$LatestRelease"
	}
}

Import-Module ([IO.Path]::Combine($SophiaFolder, 'Module', 'Sophia.psm1'))
Start-Sleep -Second 1

. $([IO.Path]::Combine($SophiaFolder, 'Functions.ps1'))
Start-Sleep -Second 1
