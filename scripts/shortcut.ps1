Import-Module 'win-tuning'

$Uri = 'https://gitlab.com/georglk/win-tools/-/raw/master/syspin/syspin.exe'
$Parameters = @{
	Uri       = $Uri
	DestPath  = [System.IO.Path]::Combine($env:USERPROFILE, [System.IO.Path]::GetFileName($Uri))
	TempPath  = [System.IO.Path]::Combine($env:TEMP, [System.IO.Path]::GetFileName($Uri))
}

Copy-FromUri @Parameters

"==> Pin to taskbar `"Show Desktop`"" | Write-Host  -ForegroundColor Yellow
New-Shortcut -Path "$env:USERPROFILE\Show Desktop.lnk" -TargetPath "%windir%\explorer.exe" -Arguments "Shell:::{3080F90D-D7AD-11D9-BD98-0000947B0257}" -IconLocation "%windir%\System32\shell32.dll,34" -Description "Show Desktop"
Start-Process -Wait -WindowStyle Hidden -FilePath "$env:USERPROFILE\syspin.exe" -ArgumentList "`"$env:USERPROFILE\Show Desktop.lnk`"","5386"

"==> Pin to taskbar `"Command Prompt`"" | Write-Host  -ForegroundColor Yellow
New-Shortcut -Path "$env:USERPROFILE\Command Prompt.lnk" -TargetPath "%windir%\system32\cmd.exe" -WorkDir "%HOMEDRIVE%%HOMEPATH%" -IconLocation "%windir%\system32\cmd.exe,0" -Description "Используется для выполнения вводимых с клавиатуры команд."
Start-Process -Wait -WindowStyle Hidden -FilePath "$env:USERPROFILE\syspin.exe" -ArgumentList "`"$env:USERPROFILE\Command Prompt.lnk`"","5386"

"==> Pin to taskbar `"Windows PowerShell ISE`"" | Write-Host  -ForegroundColor Yellow
New-Shortcut -Path "$env:USERPROFILE\Windows PowerShell ISE.lnk" -TargetPath "%windir%\system32\WindowsPowerShell\v1.0\PowerShell_ISE.exe" -WorkDir "%HOMEDRIVE%%HOMEPATH%" -IconLocation "%windir%\system32\WindowsPowerShell\v1.0\powershell_ise.exe,0" -Description "Интегрированная среда сценариев Windows PowerShell. Выполнение операций с объектами (командная строка)"
Start-Process -Wait -WindowStyle Hidden -FilePath "$env:USERPROFILE\syspin.exe" -ArgumentList "`"$env:USERPROFILE\Windows PowerShell ISE.lnk`"","5386"

"==> Pin to taskbar `"Windows PowerShell`"" | Write-Host  -ForegroundColor Yellow
New-Shortcut -Path "$env:USERPROFILE\Windows PowerShell.lnk" -TargetPath "%windir%\system32\WindowsPowerShell\v1.0\powershell.exe" -WorkDir "%HOMEDRIVE%%HOMEPATH%" -IconLocation "%windir%\system32\WindowsPowerShell\v1.0\powershell.exe,0" -Description "Performs object-based (command-line) functions"
Start-Process -Wait -WindowStyle Hidden -FilePath "$env:USERPROFILE\syspin.exe" -ArgumentList "`"$env:USERPROFILE\Windows PowerShell.lnk`"","5386"

$Uri = 'https://gitlab.com/georglk/sysinternals/-/raw/master/Procmon.exe'
$Parameters = @{
	Uri       = $Uri
	DestPath  = [System.IO.Path]::Combine($env:USERPROFILE, [System.IO.Path]::GetFileName($Uri))
	TempPath  = [System.IO.Path]::Combine($env:TEMP, [System.IO.Path]::GetFileName($Uri))
}

Copy-FromUri @Parameters

"==> Pin to taskbar `"Process Monitor`"" | Write-Host  -ForegroundColor Yellow
New-Shortcut -Path "$env:USERPROFILE\Process Monitor.lnk" -TargetPath "$env:USERPROFILE\Procmon.exe" -IconLocation "$env:USERPROFILE\Procmon.exe,0" -Description "Sysinternals Process Monitor" -WorkDir "%HOMEDRIVE%%HOMEPATH%"
Start-Process -Wait -WindowStyle Hidden -FilePath "$env:USERPROFILE\syspin.exe" -ArgumentList "`"$env:USERPROFILE\Process Monitor.lnk`"","5386"

$Uri = 'https://gitlab.com/georglk/sysinternals/-/raw/master/procexp.exe'
$Parameters = @{
	Uri       = $Uri
	DestPath  = [System.IO.Path]::Combine($env:USERPROFILE, [System.IO.Path]::GetFileName($Uri))
	TempPath  = [System.IO.Path]::Combine($env:TEMP, [System.IO.Path]::GetFileName($Uri))
}

Copy-FromUri @Parameters

"==> Pin to taskbar `"Process Explorer`"" | Write-Host  -ForegroundColor Yellow
New-Shortcut -Path "$env:USERPROFILE\Process Explorer.lnk" -TargetPath "$env:USERPROFILE\procexp.exe" -IconLocation "$env:USERPROFILE\procexp.exe,0" -Description "Sysinternals Process Explorer" -WorkDir "%HOMEDRIVE%%HOMEPATH%"
Start-Process -Wait -WindowStyle Hidden -FilePath "$env:USERPROFILE\syspin.exe" -ArgumentList "`"$env:USERPROFILE\Process Explorer.lnk`"","5386"
