#Requires -RunAsAdministrator

$Uri = 'https://dl.google.com/dl/chrome/install/googlechromestandaloneenterprise64.msi'
$Path = [System.IO.Path]::Combine($env:TEMP, [System.IO.Path]::GetFileName($Uri))

try
{
	"==> Download `"Google Chrome`" installer" | Write-Host  -ForegroundColor Yellow
	Start-BitsTransfer -Source $Uri -Destination $Path -DisplayName "Download $([System.IO.Path]::GetFileName($Uri))" -ErrorAction Stop

	"==> Install `"Google Chrome`"" | Write-Host  -ForegroundColor Yellow
	switch ([System.IO.Path]::GetExtension($Path)) {
		'.exe' {
			Start-Process -FilePath $Path -ArgumentList "/silent","/install" -Wait -Verb RunAs
		}
		'.msi' {
			Start-Process -FilePath msiexec -ArgumentList "/i", "`"$Path`"", "/qn" -Wait -Verb RunAs
		}
		Default {
			throw 'GetExtension($Path) fail'
		}
	}
}
catch
{
	[String] $_
}
finally
{
	if ([System.IO.File]::Exists($Path))
	{
		Remove-Item -Path $Path -Force
	}
}

<#
https://support.google.com/chrome/a/topic/6242754
https://dl.google.com/chrome/install/latest/chrome_installer.exe
https://dl.google.com/dl/chrome/install/googlechromestandaloneenterprise64.msi
#>
