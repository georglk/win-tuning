Import-Module 'win-tuning'

# Sound scheme
$Path = "Registry::HKCU\AppEvents\Schemes"
$Key = "(Default)"
$Value = ".None"

if (-not (Test-Path -Path $Path)) {
	New-item -Path $Path -Force | Out-Null
}

if ((Get-RegistryKeyValue -Path $Path -Name $Key) -ne $Value) {
	"==> Set Sound Schemes to 'No Sound'" | Write-Host -ForegroundColor Yellow
	New-itemProperty -Path $Path -Name $Key -Value $Value -Force | Out-Null
	Get-ChildItem -Path "Registry::HKCU\AppEvents\Schemes\Apps" |
		Get-ChildItem |
		Get-ChildItem |
		Where-Object { $_.PSChildName -eq ".Current" } |
		Set-ItemProperty -Name $Key -Value ""
}
