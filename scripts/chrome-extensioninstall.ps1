#Requires -RunAsAdministrator

Import-Module 'win-tuning'

# ExtensionInstallForcelist - uBlock Origin
$Parameters = @{
	Name  = '1'
	Value = 'cjpalhdlnbpafiamejdnhcphjbkeiagm'
	Type  = 'String'
	Path  = 'Registry::HKCU\SOFTWARE\Policies\Google\Chrome\ExtensionInstallForcelist'
}

Show-Result $Parameters $(Set-RegistryKeyValue @Parameters)

# ExtensionInstallForcelist - Bitwarden
$Parameters = @{
	Name  = '2'
	Value = 'nngceckbapebfimnlniiiahkandclblb'
	Type  = 'String'
	Path  = 'Registry::HKCU\SOFTWARE\Policies\Google\Chrome\ExtensionInstallForcelist'
}

Show-Result $Parameters $(Set-RegistryKeyValue @Parameters)

# ExtensionInstallForcelist - TeamCity Notifier
$Parameters = @{
	Name  = '3'
	Value = 'miolcigeeebinhdbihpodaajenfoggjl'
	Type  = 'String'
	Path  = 'Registry::HKCU\SOFTWARE\Policies\Google\Chrome\ExtensionInstallForcelist'
}

Show-Result $Parameters $(Set-RegistryKeyValue @Parameters)