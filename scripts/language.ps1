#Requires -RunAsAdministrator

Import-Module 'win-tuning'

if (-not ("Russian" -in $(Get-WinUserLanguageList).EnglishName)) {
	# Sets the language list and associated properties for the current user account.
	"==> Set-WinUserLanguageList `"ru-RU`"" | Write-Host -ForegroundColor Yellow
	$WinUserLanguageList = Get-WinUserLanguageList
	$WinUserLanguageList.Add("ru-RU")
	Set-WinUserLanguageList -LanguageList $WinUserLanguageList -Force
	Start-Sleep -Second 1
}

if ($(Get-WinHomeLocation).GeoId -ne 203) {
	# Sets the home location setting for the current user account.
	"==> Set-WinHomeLocation `"Россия`"" | Write-Host -ForegroundColor Yellow
	Set-WinHomeLocation -GeoId 203
	Start-Sleep -Second 1
}

if ($(Get-WinDefaultInputMethodOverride).InputMethodTip -ne "0409:00000409") {
	# Sets the default input method override for the current user account.
	"==> Set-WinDefaultInputMethodOverride `"Английский (США) - США`"" | Write-Host -ForegroundColor Yellow
	Set-WinDefaultInputMethodOverride -InputTip "0409:00000409"
	Start-Sleep -Second 1
}

# Раскладка клавиатуры
$RegistryPath = @{
	Path = 'Registry::HKCU\Keyboard Layout\Toggle'
}

$Hotkey = 2
$LanguageHotKey = 2
$LayoutHotKey = 2

$action = @{}

if ((Get-RegistryKeyValue @RegistryPath -Name 'Hotkey') -ne $Hotkey) {
	$action.Add('Hotkey', $Hotkey)
}

if ((Get-RegistryKeyValue @RegistryPath -Name 'Language HotKey') -ne $LanguageHotKey) {
	$action.Add('Language HotKey', $LanguageHotKey)
}

if ((Get-RegistryKeyValue @RegistryPath -Name 'Layout HotKey') -ne $LayoutHotKey) {
	$action.Add('Layout HotKey', $LayoutHotKey)
}

if ($action.Count -gt 0) {
	"==> Set Ctrl + Shift" | Write-Host -ForegroundColor Yellow
	$action.GetEnumerator() | ForEach-Object {
		Write-Output "set '$($_.Key)' = $($_.Value)" | Write-Host -ForegroundColor Yellow
		try {
			Set-ItemProperty @RegistryPath -Name $_.Key -Value $_.Value -Type 'String'
		}
		catch {
			$_
		}
	}
}

# Set-ItemProperty -Path 'Registry::HKU\.DEFAULT\Keyboard Layout\Toggle' -Name 'Hotkey' -Value '2'
# Set-ItemProperty -Path 'Registry::HKU\.DEFAULT\Keyboard Layout\Toggle' -Name 'Language HotKey' -Value '2'
# Set-ItemProperty -Path 'Registry::HKU\.DEFAULT\Keyboard Layout\Toggle' -Name 'Layout HotKey' -Value '2'

if ($(Get-Culture).Name -ne "ru-RU") {
	# Sets the user culture for the current user account.
	"==> Set-Culture `"ru-RU`"" | Write-Host -ForegroundColor Yellow
	Set-Culture -CultureInfo "ru-RU"
	Start-Sleep -Second 1
	# Changes made by the use of this cmdlet will take effect on subsequent PowerShell sessions.
	"Changes take effect after relaunch PowerShell session" | Write-Host -ForegroundColor Red -NoNewline
	if (-not [string]::IsNullOrEmpty($PSCommandPath)) {
		" " | Write-Host -NoNewline
		1..3 | ForEach-Object { "." | Write-Host -ForegroundColor Red -NoNewline; Start-Sleep -Second 1 }
		" " | Write-Host -NoNewline
		"Relaunch" | Write-Host -ForegroundColor DarkRed
		Get-Process -Id $PID | Select-Object -ExpandProperty Path | ForEach-Object {
			Start-Process -FilePath "$_" -ArgumentList "-NoLogo","-File `"$PSCommandPath`"" -NoNewWindow
		}
		exit
	}
	"" | Write-Host
}

if ($(Get-WinSystemLocale).Name -ne "ru-RU") {
	# Sets the system locale for the current computer.
	"==> Set-WinSystemLocale `"ru-RU`"" | Write-Host -ForegroundColor Yellow
	Set-WinSystemLocale -SystemLocale "ru-RU"
	Start-Sleep -Second 1
	# This is a system setting. It can only be changed by a user who has Administrator permissions. Changes take effect after the computer is restarted.
	"Changes take effect after the computer is restarted" | Write-Host -ForegroundColor Red -NoNewline
	" " | Write-Host -NoNewline
	1..3 | ForEach-Object { "." | Write-Host -ForegroundColor Red -NoNewline; Start-Sleep -Second 1 }
	" " | Write-Host -NoNewline
	"Restart" | Write-Host -ForegroundColor DarkRed
	Start-Sleep -Second 1
	Restart-Computer
}
