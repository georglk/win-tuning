#Requires -RunAsAdministrator

Import-Module 'win-tuning'

$collection = @(
	"bmp", "dib", "gif", "jfif", "jpe", "jpeg", "jpg", "png", "tif", "tiff", "wdp"
)

foreach ($item in $collection)
{
	$Path = "Registry::HKEY_CLASSES_ROOT\SystemFileAssociations\." + $item + "\Shell\setdesktopwallpaper"
	$Result = Remove-RegistryKey -Path $Path

	if ([string]::IsNullOrEmpty(($Result)))
	{
		"==> Remove SetDesktopWallpaper for $item ... Error" | Write-Host -ForegroundColor Red
	}
	elseif ($Result)
	{
		"==> Remove SetDesktopWallpaper for $item ... OK" | Write-Host
	}
}
