[System.Uri]$Uri = 'https://code.visualstudio.com/sha/download?build=stable&os=win32-x64-user'
$Path = [System.IO.Path]::Combine($env:TEMP, 'vscode-install.exe')

try
{
	"==> Download `"Visual Studio Code`" installer" | Write-Host  -ForegroundColor Yellow
	Start-BitsTransfer -Source $Uri -Destination $Path -DisplayName "Download Visual Studio Code" -ErrorAction Stop

	"==> Install `"Visual Studio Code`"" | Write-Host  -ForegroundColor Yellow
	Start-Process -FilePath $Path -ArgumentList "/VERYSILENT","/NORESTART","/MERGETASKS=!runcode,addcontextmenufiles" -Wait -ErrorAction Stop
}
catch
{
	[String] $_
}
finally
{
	if ([System.IO.File]::Exists($Path))
	{
		Remove-Item -Path $Path -Force
	}
}
