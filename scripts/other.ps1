Import-Module 'win-tuning'

# Use Large or Small Taskbar
$Parameters = @{
	Name  = 'TaskbarSmallIcons'
	Value = 1
	Type  = 'String'
	Path  = 'Registry::HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced'
}

switch (Set-RegistryKeyValue @Parameters)
{
	$true
	{
		"==> Set Use Large or Small Taskbar ... OK" | Write-Host -ForegroundColor Yellow
		Restart-Explorer
	}
	$null
	{
		"==> Set Use Large or Small Taskbar ... Error" | Write-Host -ForegroundColor Red
	}
}

# Группировать кнопки на панели задач (Combine taskbar buttons): При переполнении панели задач (When taskbar is full)
$Parameters = @{
	Name  = 'TaskbarGlomLevel'
	Value = 1
	Type  = 'DWord'
	Path  = 'Registry::HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced'
}

switch (Set-RegistryKeyValue @Parameters)
{
	$true
	{
		"==> Set Combine taskbar buttons to When taskbar is full ... OK" | Write-Host -ForegroundColor Yellow
		Restart-Explorer
	}
	$null
	{
		"==> Set Combine taskbar buttons to When taskbar is full ... Error" | Write-Host -ForegroundColor Red
	}
}

# Hide Folders from This PC
Hide-FoldersfromThisPC -folder 'Music' -Hide
Hide-FoldersfromThisPC -folder 'Videos' -Hide
Hide-FoldersfromThisPC -folder '3DObjects' -Hide
