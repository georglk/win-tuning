#Requires -RunAsAdministrator

Import-Module 'win-tuning'

# RestoreOnStartup
$Parameters = @{
	Name  = 'RestoreOnStartup'
	Value = 5
	Type  = 'DWord'
	Path  = 'Registry::HKCU\SOFTWARE\Policies\Microsoft\Edge'
}

Show-Result $Parameters $(Set-RegistryKeyValue @Parameters)

# HideFirstRunExperience
$Parameters = @{
	Name  = 'HideFirstRunExperience'
	Value = 1
	Type  = 'String'
	Path  = 'Registry::HKCU\SOFTWARE\Policies\Microsoft\Edge'
}

Show-Result $Parameters $(Set-RegistryKeyValue @Parameters)

# NewTabPageQuickLinksEnabled
$Parameters = @{
	Name  = 'NewTabPageQuickLinksEnabled'
	Value = 0
	Type  = 'DWord'
	Path  = 'Registry::HKCU\SOFTWARE\Policies\Microsoft\Edge'
}

Show-Result $Parameters $(Set-RegistryKeyValue @Parameters)

# FavoritesBarEnabled
$Parameters = @{
	Name  = 'FavoritesBarEnabled'
	Value = 0
	Type  = 'DWord'
	Path  = 'Registry::HKCU\SOFTWARE\Policies\Microsoft\Edge'
}

Show-Result $Parameters $(Set-RegistryKeyValue @Parameters)

<#
https://learn.microsoft.com/en-us/deployedge/microsoft-edge-policies
#>
