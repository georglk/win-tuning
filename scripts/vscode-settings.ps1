Import-Module 'win-tuning'

$vsCodeExec = [System.IO.Path]::Combine($env:LOCALAPPDATA, 'Programs\Microsoft VS Code\Code.exe')

if ([System.IO.File]::Exists($vsCodeExec))
{
	$Uri = 'https://gitlab.com/georglk/win-tools/-/raw/master/syspin/syspin.exe'
	$Parameters = @{
		Uri       = $Uri
		DestPath  = [System.IO.Path]::Combine($env:USERPROFILE, [System.IO.Path]::GetFileName($Uri))
		TempPath  = [System.IO.Path]::Combine($env:TEMP, [System.IO.Path]::GetFileName($Uri))
	}

	Copy-FromUri @Parameters

	"==> Pin to taskbar `"Visual Studio Code`"" | Write-Host  -ForegroundColor Yellow
	New-Shortcut -Path "$env:USERPROFILE\Visual Studio Code.lnk" -TargetPath $vsCodeExec -IconLocation $vsCodeExec -Description "Visual Studio Code (VS Code)" -WorkDir "%HOMEDRIVE%%HOMEPATH%"
	Start-Process -Wait -WindowStyle Hidden -FilePath "$env:USERPROFILE\syspin.exe" -ArgumentList "`"$env:USERPROFILE\Visual Studio Code.lnk`"","5386"
}

$vsCodeCMD = [System.IO.Path]::Combine($env:LOCALAPPDATA, 'Programs\Microsoft VS Code\bin\code.cmd')

$extensions = @(
	"ms-vscode.powershell"
) | Sort-Object

"==> Install extensions `"Visual Studio Code`":" | Write-Host  -ForegroundColor Yellow
$extensions | ForEach-Object {
	try {
		Invoke-Expression "& '$vsCodeCMD' --install-extension $_ --force"
	} catch {
		[string]$_
	}
}

"==> List installed extensions `"Visual Studio Code`":" | Write-Host  -ForegroundColor Yellow
try {
	Invoke-Expression "& '$vsCodeCMD' --list-extensions --show-versions"
}
catch {
	[string]$_
}
